#!/usr/bin/env python
# coding: utf-8
# %%
import torch
from torch import nn
from torchvision.models.vgg import vgg16


# %%
class GeneratorLoss(nn.Module):
    def __init__(self, cuda):
        super(GeneratorLoss, self).__init__()
        vgg = vgg16(pretrained=True)
        loss_network = nn.Sequential(*list(vgg.features)[:31]).eval().to(cuda)
        for param in loss_network.parameters():
            param.requires_grad=False
        self.loss_network = loss_network
        self.mse_loss = nn.MSELoss()
        self.tv_loss = TVLoss()
        self.bce_loss = nn.BCELoss()
        self.cuda=cuda
        
    def forward(self, out_labels, out_images, target_images, weights):
        loss_gen = self.bce_loss(out_labels, torch.full((out_images.shape[0],), 1, dtype=out_images.dtype, device=self.cuda))
        loss_vgg = self.mse_loss(self.loss_network(out_images), self.loss_network(target_images))
        if weights[0] > 0:
            loss_mse = self.mse_loss(out_images, target_images)
        else: loss_mse = 0
        tv_loss = self.tv_loss(out_images)
        #print(str(10*loss_mse) + " " + str(loss_gen) + " " + str(loss_vgg) + " " + str(1e-4*tv_loss))
        return weights[0]*loss_mse + weights[1]*loss_gen + weights[2]*loss_vgg + weights[3]*tv_loss

# %%

class TVLoss(nn.Module):
    def __init__(self, tv_loss_weight=1):
        super(TVLoss, self).__init__()
        self.tv_loss_weight=tv_loss_weight
    
    def forward(self, x):
        tv_height = torch.pow(x[:,:,1:,:] - x[:,:,:-1, :], 2).sum()
        tv_width = torch.pow(x[:,:,:,1:] - x[:,:,:,:-1], 2).sum()
        return self.tv_loss_weight*(tv_height + tv_width)