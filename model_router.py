from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/enhance')
def enhance_image(image_dto):
    return 'Enhance'
