import os
import torch
import torchvision
from torch.utils.data import Dataset
from PIL import Image
from IPython.display import display
import numpy as np
from matplotlib import pyplot as plt
from albumentations import Compose, OneOf, Flip, Resize, RandomCrop, CenterCrop
import cv2

class BSD300(Dataset):
    def __init__(self, root):
        #Use both train and test split for testing
        train_img_dir = root+'/train'
        test_img_dir = root+'/test'
        train_images = [os.path.join(train_img_dir, file_name) for file_name in os.listdir(train_img_dir)]
        test_images = [os.path.join(test_img_dir, file_name) for file_name in os.listdir(test_img_dir)]
        self.images = train_images + test_images
        
    def __getitem__(self, index):
        hr_im = np.array(Image.open(self.images[index]).convert('RGB')) #Load image
        
        #Crop so it's side lengths are divisible by 4
        crop_w = hr_im.shape[0] - hr_im.shape[0]%4
        crop_h = hr_im.shape[1] - hr_im.shape[1]%4
        crop = CenterCrop(crop_w, crop_h, p=1)
        #Downscale with inter_cubic
        resize = Resize(int(crop_w/4), int(crop_h/4), interpolation=cv2.INTER_CUBIC, p=1)
        
        #Do the cropping/Downscaling
        data = {'image': hr_im}
        data_cropped = crop(**data)
        data_downscaled = resize(**data_cropped)
        
        #Get the images and return them
        hr_im = torch.from_numpy(data_cropped['image']).permute(2,0,1).type(torch.float)/255
        lr_im = torch.from_numpy(data_downscaled['image']).permute(2,0,1).type(torch.float)/255
        
        return hr_im, lr_im
        
    def __len__(self):
        return len(self.images)