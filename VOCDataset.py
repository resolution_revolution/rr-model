#!/usr/bin/env python
# coding: utf-8
# %%
import os
import torch
import torchvision
from torch.utils.data import Dataset
from PIL import Image
from IPython.display import display
import numpy as np
from matplotlib import pyplot as plt
from albumentations import Compose, OneOf, Flip, Resize, RandomCrop, CenterCrop
import cv2


# %%
class VOCDataset(Dataset):
    def __init__(self, root, split):
        self.img_dir = root
        self.split=split
        if self.split == 'train':
            self.images = [os.path.join(self.img_dir, file_name) for file_name in os.listdir(self.img_dir) if file_name[:4] != '2007']
            self.crop = Compose([RandomCrop(300,300, p=1.0), Flip(p=0.5),])
        elif self.split == 'validation':
            self.images = [os.path.join(self.img_dir, file_name) for file_name in os.listdir(self.img_dir) if file_name[:4] == '2007']
            self.crop = Compose([CenterCrop(300,300, p=1.0),])
        else:
            print("Invalid split. Choose between 'train' and 'test'")
            return -1
        self.resize = OneOf([
            Resize(75,75, interpolation=cv2.INTER_NEAREST, p=1),
            Resize(75,75, interpolation=cv2.INTER_LINEAR, p=1),
            Resize(75,75, interpolation=cv2.INTER_CUBIC, p=1),
            Resize(75,75, interpolation=cv2.INTER_AREA, p=1),
            Resize(75,75, interpolation=cv2.INTER_LANCZOS4, p=1),
        ], p=1.0)
        
        
    def __getitem__(self, index):
        im = np.array(Image.open(self.images[index]).convert('RGB'))
        data = {'image': im}
        data_cropped = self.crop(**data)
        data_downscaled = self.resize(**data_cropped)
        im_cropped = torch.from_numpy(data_cropped['image']).permute(2,0,1).type(torch.float)/255
        im_downscaled = torch.from_numpy(data_downscaled['image']).permute(2,0,1).type(torch.float)/255
        return im_cropped, im_downscaled
        
    def __len__(self):
        return len(self.images)

